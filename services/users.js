const bcrypt = require('bcrypt');
const v4 = require('uuid').v4;

import User from '../models/user'

export const findUserByEmail = async (email) => {
    const userFound = await User.findOne({email});
    return userFound;
}  

export const findUserById = async (id) => {
    const user = await User.findById(id).populate('tasks');
    return user;
}  

export const createUser = async (email, password) => {
    const hash = await bcrypt.hash(password, 6)
    const user = new User({
        _id: v4(),
        email,
        password: hash,
    });
    const userCreated = await user.save();
    return userCreated;
}

export const passwordMatches = async (password, hash) => {
    let match = await bcrypt.compare(password, hash);
    return match;
}
