import User from '../models/user'
import Task from '../models/task'

export const findTaskById = async (id) => {
    const taskFound = await Task.findById(id);
    return taskFound;
}

export const updateTaskById = async (id, values) => {
    const taskUpdated = await Task.findByIdAndUpdate(id, values, {new: true});
    return taskUpdated;
}

export const addTask = async (userId, value) => {
    const task = new Task({
        text: value
    });
    const taskCreated = await task.save();
    const userUpdated = await User.findByIdAndUpdate(userId, { $push: { 'tasks': taskCreated} }, {new: true}).populate('tasks');
    return userUpdated;
}

export const deleteDoneTasks = async () => {
    let deleteStats = await Task.deleteMany({done: true});
    return;
}

export const deleteTask = async (taskId) => {
    let deleteStats = await Task.findByIdAndRemove(taskId);
    return;
}

