import {
    SET_USER, 
    SET_TASKS,
    ERROR,
} from '../constants/userConstants'

import { 
    getTodos, 
    addTodo, 
    updateTodo,
    clearDos,
    deleteTodo
} from '../services/todosServices';

const getTodosAction = () => {
    return async (dispatch, getState) => {
        try {
            const token = getState().user.token;
            if(!token){
                return dispatch({ type: ERROR, payload: e.message });
            }

            const response = await getTodos(token);   
            dispatch({ type: SET_USER, payload: response?.user });
        } catch (e) {
            dispatch({ type: ERROR, payload: e.message });
        }
    };
};

const addTodoAction = (
    value,
) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().user.token;
            if(!token){
                return dispatch({ type: ERROR, payload: e.message });
            }
            const response = await addTodo(token, value);    
            dispatch({ type: SET_TASKS, payload: response?.tasks });
        } catch (e) {
            dispatch({ type: ERROR, payload: e.message });
        }
    };
};

const updateTodoAction = (
    task,
) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().user.token;
            if(!token){
                return dispatch({ type: ERROR, payload: e.message });
            }

            const response = await updateTodo(token, task._id, task);    
            dispatch({ type: SET_TASKS, payload: response?.tasks });
        } catch (e) {
            dispatch({ type: ERROR, payload: e.message });
        }
    };
};

const clearDosAction = () => {
    return async (dispatch, getState) => {
        try {
            const token = getState().user.token;
            if(!token){
                return dispatch({ type: ERROR, payload: e.message });
            }

            const response = await clearDos(token);    
            dispatch({ type: SET_TASKS, payload: response?.tasks });
        } catch (e) {
            dispatch({ type: ERROR, payload: e.message });
        }
    };
};

const deleteTodoAction = (
    taskId,
) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().user.token;
            if (!token) {
                return dispatch({ type: ERROR, payload: e.message });
            }

            const response = await deleteTodo(token, taskId); 
            dispatch({ type: SET_TASKS, payload: response?.tasks });
        } catch (e) {
            dispatch({ type: ERROR, payload: e.message });
        }
    };
};


export {
    getTodosAction,
    addTodoAction,
    updateTodoAction,
    clearDosAction,
    deleteTodoAction,
}