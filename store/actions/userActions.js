import { 
    SET_TOKEN,
    SET_USER,
    ERROR,
    LOGOUT,
} from '../constants/userConstants'

import {
    userRegister,
    userLogin,
} from '../services/userServices'

import errorConstants from '../../utils/errorConstants';

const signUpAction = ({
    email,
    password,
    repeatPassword,
}) => {
    return async dispatch => {
        try {
            if(repeatPassword != password) dispatch({ 
                type: ERROR, 
                payload: errorConstants['PASSWORDS_DONT_MATCH']
            });

            if(password.length < 3) dispatch({ 
                type: ERROR, 
                payload: errorConstants['PASSWORD_TOO_SHORT'] 
            });

            const response = await userRegister(email, password);            
            dispatch({ type: SET_TOKEN, payload: response?.token });
            dispatch({ type: SET_USER, payload: response?.user });
        } catch (e) {
            dispatch({ type: ERROR, payload: e.message });
        } /* finally {
        } */
    };
};


const signInAction = ({
    email,
    password,
}) => {
    return async dispatch => {
        try {
            const response = await userLogin(email, password);    
            dispatch({ type: SET_TOKEN, payload: response?.token });        
            dispatch({ type: SET_USER, payload: response?.user });
        } catch (e) {
            dispatch({ type: ERROR, payload: e.message });
        } /* finally {
        } */
    };
};

const logoutAction = () => ({
    type: LOGOUT
})


export {
    signUpAction,
    signInAction,
    logoutAction,
}