
import userActions from './userActions';
import todosActions from './todosActions';

export default {
  ...userActions,
  ...todosActions,
};