import { USER_INITIAL_STATE, LOGOUT, SET_TOKEN, SET_USER, SET_TASKS, ERROR } from '../constants/userConstants.js';

const userReducer = (state = USER_INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_TOKEN:
            return {
                ...state,
                token: action.payload
            }

        case SET_USER:
            return {
                ...state,
                user: action.payload
            }

        case SET_TASKS:
            return {
                ...state,
                user: {
                    ...state.user,
                    tasks: action.payload
                }
            }

        case LOGOUT:
            return {
                ...state,
                token: null
            }

        case ERROR:
            return {
                ...state,
                error: action.payload
            }
            
        default:
            return state;
    }
};

export default userReducer;