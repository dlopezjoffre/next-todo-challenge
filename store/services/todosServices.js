
import { getAuthRequest, jsonAuthRequest } from '.'
import errorConstants from '../../utils/errorConstants';

const {
    REQUEST_NOT_VALID,
} = errorConstants;

export const getTodos = async (token) => {
    const getRequest = getAuthRequest(token, 'GET');
    try{
        const data = await fetch(`/api/todos`, { 
            ...getRequest,
        });
        return data.json();
    } catch (e) {
        throw { message: e.response?.data?.messageId || REQUEST_NOT_VALID };
    }
} 

export const addTodo = async (token, value) => {
    const postRequest = jsonAuthRequest(token, 'POST');
    try{
        const data = await fetch(`/api/todos`, { 
            ...postRequest,
            body: JSON.stringify({
                text: value
            }),
        });
        return data.json();
    } catch (e) {
        throw { message: e.response?.data?.messageId || REQUEST_NOT_VALID };
    }
}

export const updateTodo = async (token, id, task) => {
    const putRequest = jsonAuthRequest(token, 'PUT');
    try{
        const data = await fetch(`/api/todos/${id}`, { 
            ...putRequest,
            body: JSON.stringify(task),
        });
        return data.json();
    } catch (e) {
        throw { message: e.response?.data?.messageId || REQUEST_NOT_VALID };
    }
}

export const clearDos = async (token) => {
    const getRequest = getAuthRequest(token, 'DELETE');
    try{
        const data = await fetch(`/api/todos`, { 
            ...getRequest,
        });
        return data.json();
    } catch (e) {
        throw { message: e.response?.data?.messageId || REQUEST_NOT_VALID };
    }
}

export const deleteTodo = async (token, id) => {
    const getRequest = getAuthRequest(token, 'DELETE');
    try{
        const data = await fetch(`/api/todos/${id}`, { 
            ...getRequest,
        });
        return data.json();
    } catch (e) {
        throw { message: e.response?.data?.messageId || REQUEST_NOT_VALID };
    }
}