
import { postRequest } from '.'
import errorConstants from '../../utils/errorConstants';

const {
    REQUEST_NOT_VALID,
} = errorConstants;

export const userRegister = async (email, password) => {
    try{
        const data = await fetch('/api/users', { 
            ...postRequest,
            body: JSON.stringify({
              email,
              password,
            }),
        });
        return data.json();
    } catch (e) {
        throw { message: e.response?.data?.messageId || REQUEST_NOT_VALID };
    }
} 

export const userLogin = async (email, password) => {
    try{
        const data = await fetch('/api/users/auth', { 
            ...postRequest,
            body: JSON.stringify({
              email,
              password,
            }),
        });
        return data.json();
    } catch (e) {
        throw { message: e.response?.data?.messageId || REQUEST_NOT_VALID };
    }
} 
