export const postRequest = {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
}

export const getAuthRequest = (token, method) => {
    return {
        method: method,
        headers: {
            'Authentication': `Bearer ${token}`
        },
    }
}

export const jsonAuthRequest = (token, method) => {
    return {
        method: method,
        headers: {
            'Content-Type': 'application/json',
            'Authentication': `Bearer ${token}`
        },
    }
}
