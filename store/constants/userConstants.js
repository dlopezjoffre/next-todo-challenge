
const SIGNUP = 'SIGNUP';
const ERROR = 'ERROR';
const LOGOUT = 'LOGOUT';
const SET_TOKEN = 'SET_TOKEN';
const SET_USER = 'SET_USER';
const SET_TASKS = 'SET_TASKS';

const USER_INITIAL_STATE = {
    token: false,
    user: {},
    error: "",
}

export  {
    SIGNUP,
    ERROR,
    LOGOUT,
    SET_TOKEN,
    SET_USER,
    SET_TASKS,
    USER_INITIAL_STATE,
}
