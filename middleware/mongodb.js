const mongoose = require("mongoose");

const connectDB = handler => async (req, res) => {
  if (mongoose.connections[0].readyState) {
    return handler(req, res);
  }
  try {
    await mongoose.connect(process.env.MONGODB_URL, {
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    console.log("Succesfully connected to MongoDB client!");
  } catch (e) {
    throw e;
  }
  return handler(req, res)
};

export default connectDB;