import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const task = new Schema({
    text: {
      type: String,
      required: false
    },
    done: {
      type: Boolean,
      required: true,
      default: false
    },
    created: {
      type: Date,
      default: Date.now
    }
});

export default mongoose.models.Task || mongoose.model('Task', task);
