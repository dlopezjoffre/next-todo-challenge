import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const user = new Schema({
    _id: { 
        type: String, 
    },
    name: {
      type: String,
        required: false
    },
    email: {
      type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    tasks: [{
        type: mongoose.Schema.ObjectId,
        ref: 'Task'
    }],
    since: {
        type: Date,
        default: Date.now
    }
});

export default mongoose.models.User || mongoose.model('User', user);