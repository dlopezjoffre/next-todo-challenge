import React, { useState } from 'react';
import { connect } from "react-redux";

import Login from  '../../pages/login';
import { logoutAction } from '../../store/actions/userActions';

const ProtectedPage = ({ 
    children,
    userToken,
    logoutAction,
}) => {

	return (
        <div className="main">
        <h1 className="title">
            Welcome to <a href="https://gitlab.com/dlopezjoffre/next-todo-challenge">Todos TO-DOs!</a>
        </h1>
        <h3 className="title"> By Dolores Lopez Joffre </h3>
        {!!userToken && <p className='logoutText' onClick={logoutAction}>Logout</p>}
        {!!userToken ? 
            (children)
            : <Login/>
        }
		</div>
	)
}

const mapStateToProps = state => ({
    userToken: state.user.token
});

const mapDispatchToProps = {
    logoutAction
}

export default connect(mapStateToProps, mapDispatchToProps)(ProtectedPage);