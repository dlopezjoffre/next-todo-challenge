import React from 'react';

import styles from './button.module.scss'


const Button = ({ 
    onClick,
    text,
}) => {
	return (
        <p onClick={onClick} className={styles.button}>
            {text}
        </p>
	)
}

export default Button