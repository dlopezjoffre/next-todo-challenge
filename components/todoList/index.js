import React, { useState } from 'react';

import Button from '../button'

import List from './components/list'
import styles from './list.module.scss'

const TodoList = ({
    done,
    todo,
    onClearClick,
    onAdd,
}) => {

    const [showInput, setShowInput] = useState(false);

    const hasTodo = todo.length > 0;
    const hasDone = done.length > 0;

    const ButtonAdd = () => (
        <Button onClick={() => setShowInput(true)} text={'Add'}/>
    )

    const ButtonClear = () => (
        <Button onClick={onClearClick} text={'Clear'}/>
    )
    
    return (
    <div className={styles.container}>

        <List
            containerStyles={styles.todoListContainer + " " + (!hasDone && styles.roundedCorners)}
            headerStyles={!hasTodo ? styles.headerEmpty : ''}
            title={'To Do'}
            ListButton={ButtonAdd}
            data={todo}
            emptyText={'No pending tasks :D'}
            showAddInput={showInput}
            onAdd={onAdd}
            onBlur={() => setShowInput(false)}
        />

        <List
            containerStyles={styles.doneListContainer}
            headerStyles={!hasDone ? styles.headerEmpty : ''}
            title={'done'}
            ListButton={ButtonClear}
            data={done}
            emptyText={'Anything done yet? :('}
            completed
        />
    </div>       
    );
}

TodoList.defaultProps = {
    onClearClick: () => {},
    onAdd: () => {},
};

export default TodoList;