import React, { useState, useRef, useEffect } from 'react';
import { connect } from "react-redux";

import { updateTodoAction, deleteTodoAction } from '../../../store/actions/todosActions';

import TodoItem from './todoItem'
import styles from '../list.module.scss'


const List = ({
    containerStyles,
    headerStyles,
    title,
    ListButton,
    emptyText,
    data,
    completed,
    updateTodoAction,
    deleteTodoAction,
    showAddInput,
    onAdd,
    onBlur,
}) => {
    const [text, setText] = useState('');
    let addInput = useRef(null);

    useEffect(() => {
        if(showAddInput && !!addInput) addInput.current.focus()
    }, [showAddInput])

    const onTextChange = (evt) => {
        setText(evt.target.value);
    }

    const handleAdd = (evt) => {
        if(evt.key === 'Enter'){
            if(text.length > 0) {
                onAdd(text);
                setText('');
            } else {
                onBlur()
            }
        }
    }

    const EmptyListComponent = ({ text }) => {return(
        <p className={'normalText'}> {text} </p>
    )}

    return(
        <div className={styles.list + " " + containerStyles}>
            <div className={styles.header + " " + headerStyles}>
                { !!showAddInput ?
                    <input
                        ref={addInput}
                        type="text"
                        className={'addInput'}
                        value={text}
                        onBlur={(e) => onBlur(e)}
                        onChange={(e) => onTextChange(e)}
                        onKeyPress={(e) => handleAdd(e)}
                    />
                    : <h3 className={styles.title + " " + (!completed && styles.black)}>{title}</h3>
                }
                <ListButton/>
            </div>
            <div className={styles.listContent}>
            { data.length > 0 ?
                data.map((item, index) => (
                    <TodoItem
                    key={index+item.text}
                    index={index}
                    todoText={item.text}
                    completed={item.done}
                    onToggleTodo={() => updateTodoAction({...item, done:!item.done})}
                    onEditTodo={(text) => updateTodoAction({...item, text})}
                    onRemoveTodo={() => deleteTodoAction(item._id)}
                    />
                ))
                : <EmptyListComponent text={emptyText}/>
            }
            </div>
        </div>
    )
}

List.defaultProps = {
    headerStyles: '',
    completed: false,
    showAddInput: false, 
    onAdd: () => {},
    onBlur: () => {},
}

const mapDispatchToProps = {
  updateTodoAction,
  deleteTodoAction,  
};

export default connect(null, mapDispatchToProps)(List);