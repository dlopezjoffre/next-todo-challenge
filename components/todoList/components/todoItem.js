import React, { useState } from 'react';
//import { CIcon } from '@coreui/icons-react';

import { XCircle, CheckCircle, Circle } from 'react-feather';

import styles from './item.module.scss'

const TodoItem = ({
    index,
    todoText,
    completed,
    onToggleTodo,
    onEditTodo,
    onRemoveTodo,
}) => {
    const [text, setText] = useState(todoText);
    
    const onTodoChange = (evt) => {
        setText(evt.target.value);
    }
    
    const onBlurEditing = (evt) => {
        if (text.length > 0) {
            onEditTodo(text);
        }
    }

    const handleEdit = (evt) => {
      if(evt.key === 'Enter' && text.length > 0){
        onEditTodo(text);
      }
    }
 
    return (
        <div className={styles.container}>
            <div onClick={onToggleTodo}>
                {completed ?
                    <CheckCircle className={styles.icon}/>
                    : <Circle className={styles.icon}/>
                } 
            </div>

            <input
                type="text"
                disabled={completed}
                className={styles.input + " " + (completed && styles.doneInput)}
                value={text}
                onChange={(e) => onTodoChange(e)}
                onBlur={(e) => onBlurEditing(e)}
                onKeyPress={handleEdit}
            />
            <div onClick={onRemoveTodo}>
                <XCircle className={styles.icon + " " + styles.removeIcon}/>
            </div>
        </div>
    );
}

TodoItem.defaultProps = {
	completed: false,
};

export default TodoItem;