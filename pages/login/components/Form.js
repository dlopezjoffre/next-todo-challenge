import React, { useState } from 'react';

import styles from './form.module.scss'

const Form = ({ 
    signIn,
	error,
	onSignin,
	onSignup
}) => {

	const [details, setDetails] = useState({email: '', password: '', repeatPassword: ''});

	const submitHandler = (evt) => {
		evt.preventDefault();
		signIn ? onSignin(details) : onSignup(details);
	}

	return (
		<form className={styles.form} onSubmit={submitHandler}>
			<div className={styles.field + " " + (signIn ? styles.signIn : styles.signUp)}>
				<input id='email' name='email' type='email' placeholder='E-mail' required
				value={details.email} onChange={(e) => setDetails({...details, email: e.target.value})} />
				<input id='password' name='password' type='password' placeholder='Password' required 
				value={details.password} onChange={(e) => setDetails({...details, password: e.target.value})} />
				<input id='repeat-password' name='repeat-password' type='password' placeholder='Repeat password' required={!signIn} disabled={signIn} 
				value={details.repeatPassword} onChange={(e) => setDetails({...details, repeatPassword: e.target.value})} />
			</div>
			{!!error && 
				<h1> {error} </h1>
			}
			<button className={styles.submitButton} type='submit'>
				{ signIn ? 'Sign in' : 'Sign up' }
			</button>
		</form>
	)
}

Form.defaultProps = {
	signIn: true,
	error: '',
	onSignin: () => {},
	onSignup: () => {},
};

export default Form