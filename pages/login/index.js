import React, { useState } from 'react';
import { connect } from "react-redux";

import Form from  './components/Form.js'

import styles from './login.module.scss'
import { signUpAction, signInAction } from '../../store/actions/userActions.js';

const Login = ({
    signUpAction,
    signInAction,
    authError,
}) => {
    const [signIn, setSignIn] = useState(true);

    const onSignup = (details) => {
        if(!!details.password && 
            !!details.repeatPassword && 
            !!details.email
        ) signUpAction(details);
    }

    const onSignin = (details) => {
        if(!!details.password && 
            !!details.email
        ) signInAction(details);
    }

    return (
        <div className={styles.container}>
            <header>
                <div className={styles.headings + " " + (signIn ? styles.signIn : styles.signUp)}>
                    <span>Sign in to your account</span>
                    <span>Create an account</span>
                </div>
            </header>
            <ul className={styles.options}>
                <li className={signIn ? styles.active : ''} 
                onClick={() => setSignIn(true)}>
                    Sign in
                </li>

                <li className={!signIn ? styles.active : ''} 
                onClick={() => setSignIn(false)}>
                    Sign up
                </li>
            </ul>
            <Form 
                signIn={signIn} 
                error={authError} 
                onSignup={onSignup}
                onSignin={onSignin}
            />
        </div>
    );
};

const mapStateToProps = state => ({
    authError: state.user.error,
});

const mapDispatchToProps = {
    signUpAction,
    signInAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);