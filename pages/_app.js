import App, { Container } from 'next/app';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import Head from 'next/head';

import withRedux from "next-redux-wrapper";
import { store, persistor } from '../store'; 

import ProtectedPage from '../components/protectedPage'

import '../styles/reset.css';
import '../styles/globals.scss'

const MyApp = ({Component, pageProps}) => {
  return(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Head>
        <title>ViDa-Tec TO-DOs Challenge</title>
        <meta name="description" content="Dolores Lopez Joffre - ViDa-Tec FullStack Challenge - TO-DOs NextJS app" />
      </Head>

      <ProtectedPage>
        <Component {...pageProps} />
      </ProtectedPage>
    </PersistGate>
  </Provider>
)};


export default withRedux(()=>store)(MyApp);
