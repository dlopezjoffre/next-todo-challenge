import React, { useEffect, useState } from 'react';
import { connect } from "react-redux";

import { clearDosAction, addTodoAction, getTodosAction } from '../store/actions/todosActions';

import TodoList from '../components/todoList'

const Index = ({
    user,
    error,
    getTodosAction,
    clearDosAction,
    addTodoAction,
}) => {
    const [todoList, setTodoList] = useState([]);
    const [doneList, setDoneList] = useState([]);

    const { tasks } = user || {tasks: []};

    useEffect(() => {
        getTodosAction();
    }, [])

    useEffect(() => {
        if(tasks) {
            setTodoList(tasks.filter(
                item => !item.done
            ));
    
            setDoneList(tasks.filter(
                item => item.done
            ));
        } 
    }, [tasks])



    return (
        <div className="appContainer">
            {!error ?
                <>
                    <TodoList 
                    todo={todoList} 
                    done={doneList}
                    onClearClick={clearDosAction}
                    onAdd={(text) => addTodoAction(text)}
                    />
                </>
                : <p className={'normalText'}>{error}</p>
            }
        </div>
    )
};

const mapStateToProps = state => ({
    user: state.user.user,
    error: state.user.error,
});

const mapDispatchToProps = {
    getTodosAction,
    clearDosAction,
    addTodoAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);