// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default (req, res) => {
  if (req.method === 'GET') {
    return res.status(200).json({ name: 'John Doe' })
  }
  
  if (req.method === 'POST') {
    return res.status(200).json({ email: req.body.email, password: req.body.password })
  }
}
