const assert = require('assert');

import connectDB from '../../../middleware/mongodb.js';
import { findUserByEmail, createUser } from '../../../services/users'
import { signToken } from '../../../utils/apiUtils/helpers'

const handler = async (req, res) => {
  if (req.method === 'POST') {
    //Sign Up
    try {
      assert.notStrictEqual(null, req.body.email, 'EMAIL_REQUIRED');
      assert.notStrictEqual(null, req.body.password, 'PASSWORD_REQUIRED');
    } catch (bodyError) {
      return res.status(403).json({error: true, message: bodyError.message});
    }

    const userFound = await findUserByEmail(req.body.email);
    if (userFound) {
      return res.status(403).json({error: true, message: 'EMAIL_ALREADY_IN_USE'});
    } else {
      const userCreated = await createUser(req.body.email, req.body.password);
      
      if (userCreated){
        const token = signToken(userCreated); 
        return res.status(200).json({token, user: userCreated});
      } else {
        return res.status(403).json({error: true, message: 'NO_USER_CREATED'});
      }
    }
  }

  
}

export default connectDB(handler);