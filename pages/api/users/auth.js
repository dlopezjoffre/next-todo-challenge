const assert = require('assert');

import connectDB from '../../../middleware/mongodb.js';
import { findUserByEmail, passwordMatches } from '../../../services/users'
import { signToken } from '../../../utils/apiUtils/helpers'

const handler = async (req, res) => {
  if (req.method === 'POST') {
    //Sign In
    try {
      assert.notStrictEqual(null, req.body.email, 'EMAIL_REQUIRED');
      assert.notStrictEqual(null, req.body.password, 'PASSWORD_REQUIRED');
    } catch (bodyError) {
      return res.status(403).json({error: true, message: bodyError.message});
    }

    const userFound = await findUserByEmail(req.body.email);
    if (userFound){
        if(passwordMatches(req.body.password, userFound.password)){
            const token = signToken(userFound);
            return res.status(200).json({token, user: userFound});
        } else {
            return res.status(403).json({error: true, message: 'WRONG_PASSWORD'});
        }
    } else {
        return res.status(403).json({error: true, message: 'NO_EXISTING_USER'});
    }
  }
}

export default connectDB(handler);