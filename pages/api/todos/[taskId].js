
import connectDB from '../../../middleware/mongodb.js';
import { findUserById } from '../../../services/users'
import { deleteTask, updateTaskById } from '../../../services/tasks'
  
import { verifyToken } from '../../../utils/apiUtils/helpers'
  
const handler = async (req, res) => { 
    if (req.method === 'DELETE') {
        if (req.headers.authentication) {
            try{
                const { taskId } = req.query

                const userId = verifyToken(req.headers.authentication);
                await deleteTask(taskId);
                const userUpdated = await findUserById(userId);
                if (userUpdated){
                    return res.status(200).json({tasks: userUpdated.tasks});
                } else {
                    return res.status(403).json({error: true, message: 'NO_EXISTING_USER'});
                }
            } catch (e) {
                return res.status(403).json({error: true, message: 'INVALID_TOKEN'});
            }
        } else {
            return res.status(403).json({error: true, message: 'NO_TOKEN_PROVIDED'});
        } 
    }

    if (req.method === 'PUT') {
        if (req.headers.authentication) {
            try{
                const { taskId } = req.query
    
                const userId = verifyToken(req.headers.authentication);
                const taskUpdated = await updateTaskById(taskId, req.body);
                if (taskUpdated){
                    const userFound = await findUserById(userId);
                    return res.status(200).json({tasks: userFound.tasks});
                } else {
                    return res.status(403).json({error: true, message: 'NO_EXISTING_USER'});
                }
            } catch (e) {
                return res.status(403).json({error: true, message: 'INVALID_TOKEN'});
            }            
        } else {
            return res.status(403).json({error: true, message: 'NO_TOKEN_PROVIDED'});
        }
    }
}
  
export default connectDB(handler);