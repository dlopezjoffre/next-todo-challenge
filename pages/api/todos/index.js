import connectDB from '../../../middleware/mongodb.js';
import { findUserById } from '../../../services/users'
import { addTask, deleteDoneTasks } from '../../../services/tasks'

import { verifyToken } from '../../../utils/apiUtils/helpers'

const handler = async (req, res) => {
    if (req.method === 'GET') {
        if (req.headers.authentication) {
            try{
                const userId = verifyToken(req.headers.authentication);
                const userFound = await findUserById(userId);
                if (userFound){
                    return res.status(200).json({user: userFound});
                } else {
                    return res.status(403).json({error: true, message: 'NO_EXISTING_USER'});
                }
            } catch (e) {
                return res.status(403).json({error: true, message: 'INVALID_TOKEN'});
            }   
        } else {
            return res.status(403).json({error: true, message: 'NO_TOKEN_PROVIDED'});
        }    
    }

    if (req.method === 'POST') {
        if (req.headers.authentication) {
            try{
                const userId = verifyToken(req.headers.authentication);
                const userUpdated = await addTask(userId, req.body.text)
                if (userUpdated){
                    return res.status(200).json({tasks: userUpdated.tasks});
                } else {
                    return res.status(403).json({error: true, message: 'NO_EXISTING_USER'});
                }
            } catch (e) {
                return res.status(403).json({error: true, message: 'INVALID_TOKEN'});
            }            
        } else {
            return res.status(403).json({error: true, message: 'NO_TOKEN_PROVIDED'});
        } 
    }

    if (req.method === 'DELETE') {
        if (req.headers.authentication) {
            try{
                const userId = verifyToken(req.headers.authentication);
                await deleteDoneTasks();
                const userUpdated = await findUserById(userId);

                if (userUpdated){
                    return res.status(200).json({tasks: userUpdated.tasks});
                } else {
                    return res.status(403).json({error: true, message: 'NO_EXISTING_USER'});
                }
            } catch (e) {
                return res.status(403).json({error: true, message: 'INVALID_TOKEN'});
            }            
        } else {
            return res.status(403).json({error: true, message: 'NO_TOKEN_PROVIDED'});
        } 
    }
}

export default connectDB(handler);