const errorConstants = {
    'REQUEST_NOT_VALID': 'Hubo un error, intentálo nuevamente',
    'PASSWORDS_DONT_MATCH': 'Las contraseñas no coinciden',
    'PASSWORD_TOO_SHORT': 'La contraseña debe tener mas de 3 caracteres',
    'EMAIL_ALREADY_IN_USE': 'Ya existe una cuenta con este email',
    'EMAIL_NOT_FOUND': 'No hay una cuenta con este email',
}

export default errorConstants;