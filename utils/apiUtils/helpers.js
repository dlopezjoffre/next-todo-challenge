const jwt = require('jsonwebtoken');

export const signToken = (user) => {
    let token = jwt.sign(
        {
            _id: user._id,
            email: user.email
        },
        process.env.JWT_SECRET,
        { expiresIn: 3000 },
    );
    return token;
}

export const verifyToken = (auth) => {
    if (auth.split(' ')[0] === 'Bearer') {
        let bearerToken = auth.split(' ')[1];
        try {
            let decoded = jwt.verify(bearerToken, process.env.JWT_SECRET);
            return decoded._id;
        } catch(err) {
            throw { ...err, message: 'INVALID_TOKEN' };
        }
    }
}