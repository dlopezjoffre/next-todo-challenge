// keeps first ocurrence, checks duplicate by given key
export const removeDuplicateObject = (array, key) => {
    if (isArray(array))
        return array.filter(
            (item, index, array) =>
                array.findIndex(
                    iterableObject => iterableObject[key] === item[key]
                ) === index
        );
    return [];
};