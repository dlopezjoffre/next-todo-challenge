import cookie from 'js-cookie';

export const setCookie = (key, value) => {
  if (process.browser) {
    cookie.set(key, value, {
      expires: 2,
      path: '/'
    });
  }
};

export const getCookieFromServer = (key, req) => {
  if (!req.headers.cookie) {
    return null;
  }
  const rawCookie = req.headers.cookie
    .split(';')
    .find(c => c.trim().startsWith(`${key}=`));
  if (!rawCookie) {
    return null;
  }
  return rawCookie.split('=')[1];
};